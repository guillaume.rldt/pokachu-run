# La chasse au monstre

Bouchez Alexandre Guyard Coline Roelandt Guillaume Verdiere Raphaelle 

## Présentation de la chasse au monstre

Le but de ce jeu est que : - Le chasseur arrive a tirer sur le monstre avant que celui soit passé sur toutes les cases du plateau
		       - Le monstre passe sur toutes les cases du plateau avec que le chasseur le tue

## Utilisation de la chasse au monstre

Le monstre se deplace à l'aide du pavé numérique. Exemple : la touche 7 permet de se deplacer vers le haut, la touche 2 permet de se deplacer vers le bas.

Le monstre gagne si il reussi à se déplacer sur toutes les cases du plateau et perd si il se fait toucher par le chasseur.


Le chasseur choisi une case en saisissant les coordonnées de la case (x puis y).

Si il touche une case déjà visité par le monstre, alors il saura depuis combien de tour le monstre est passé.

Le chasseur gagne si il reussi à toucher le monstre et perd si le monstre reussi à se deplacer sur toutes les cases du plateau.

Afin d'utiliser le projet, il est suffisant de taper la
commande suivante:
./launch.sh 




