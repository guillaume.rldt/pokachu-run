package projet;
/**
 * Permet de creer chaque Case du plateau
 * @author guyardc roelandg verdierr boucheza
 *
 */
public class Case {
	//attributs
	private Integer nbToursTrace;
	public boolean estDecouverte = false;
	private boolean estMalDecouverte = false;

	/**retourne true si le monstre est deja passé, false sinon
	 * 
	 * @return boolean
	 */
	public boolean estPasse() {
		return nbToursTrace != null;
	}
	/**retourne true si le monstre peut se deplacer à cet endroit et modifie la valeur de la trace du monstre, false sinon
	 * 
	 * @param nbTours int
	 * @return boolean
	 */
	public boolean passage(int nbTours) {
		if (!estPasse()) {
			nbToursTrace = nbTours;
			return true;
		}
		return false;
	}
	/**
	 * retourne le numero du tour du dernier passage sur la case
	 * @return Integer
	 */
	public Integer getNbToursTrace() {
		return nbToursTrace;
	}
	/**
	 * retourne true si le chasseur a tiré sur la case, false sinon
	 * @return boolean
	 */
	public boolean isEstDecouverte() {
		return estDecouverte;
	}
	/**
	 * retourne true si le chasseur a tiré sur la case mais le monstre n'y avait pas encore passe, false sinon
	 * @return boolean
	 */
	public boolean isEstMalDecouverte() {
		return estMalDecouverte;
	}
	/**
	 * modifier la valeur du nombre de tour ou le monstre est passe sur la case
	 * @param nbToursTrace Integer
	 */
	public void setNbToursTrace(Integer nbToursTrace) {
		this.nbToursTrace = nbToursTrace;
	}
	/**
	 * modifier la valeur si le chasseur a tire sur la case
	 * @param estDecouverte boolean
	 */
	public void setEstDecouverte(boolean estDecouverte) {
		this.estDecouverte = estDecouverte;
	}
	/**
	 * modifie la valeur si le chasseur a tire sur la case mais le monstre n'y avait pas encore passe
	 * @param estMalDecouverte boolean
	 */
	public void setEstMalDecouverte(boolean estMalDecouverte) {
		this.estMalDecouverte = estMalDecouverte;
	}
}
