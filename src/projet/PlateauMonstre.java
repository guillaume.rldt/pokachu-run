package projet;

import java.util.ArrayList;
import java.util.Random;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import objet.Bouclier;
import objet.Boue;
import objet.CafeM;
import objet.Chaussette;
import objet.Chaussure;
import objet.ObjetMonstre;

/**Cette classe gère le plateau du monstre, les déplacements du monstre et permet de savoir si le monstre a gagné ou perdu
 * 
 * @author guyardc roelandg verdierr boucheza
 *
 */
public class PlateauMonstre extends Plateau{
	//attributs
	private static Image objetFouille;
	private static int nbAction;
	protected ArrayList <ObjetMonstre> inventaire;
	//constructeur
	/**
	 * Constructeur sans parametres
	 */
	public PlateauMonstre() {
		super();
		nbAction=1;
		inventaire = new ArrayList <ObjetMonstre>();
	}
	
	/**Construit un plateau de taille nbLig * nbCol
	 * 
	 * @param nbLig int
	 * @param nbCol int
	 */
	public PlateauMonstre(int nbLig, int nbCol) {
		super(nbLig, nbCol);
		nbAction=1;
		inventaire = new ArrayList <ObjetMonstre>();

	}
	
	/**Construit un plateau de taille x * x
	 * @param x int
	 */
	public PlateauMonstre(int x) {
		super(x, x);
		nbAction=1;
		inventaire = new ArrayList <ObjetMonstre>();

	}
	
	/**Construit un plateau à partir d'un plateau déjà existant donné en parametres
	 * @param newPlateau Case [][]
	 */
	public PlateauMonstre(Case [][] newPlateau) {
		super (newPlateau);
		nbAction=1;
		inventaire = new ArrayList <ObjetMonstre>();

	}
	
	/**
	 * permet d'acceder au nombre d'action
	 * @return int
	 */
	public static int getNbAction() {
		return nbAction;
	}
	
	/**
	 * permet de changer le nombre d'action
	 * @param nbA
	 */
	public static void setNbAction(int nbA) {
		nbAction = nbA;
	}
	
	/**
	 * Recharge le nombre d'actions en fonction du tour
	 */

	public void rechargeAction() {
		nbAction = nbTour/3 + 1;
	}
	/**
	 * incremente nbAction si c'est possible
	 * @return boolean
	 */
	public boolean deincrAction() {
		if(nbAction > 0) {
			nbAction--;
			return true;
		}
		return false;
	}
	/**
	 * permet d'ajouter un objet dans l'inventaire
	 * @param o
	 */
	public void ajoutObj(ObjetMonstre o) {
		inventaire.add(o);
	}
	/**
	 * permet de retirer un objet de l'inventaire
	 * @param o
	 */
	public void retireObj(ObjetMonstre o) {
		if(o.expire())
			inventaire.remove(o);
	}
	/**
	 * Permet de fouiller une case et d'avoir un nouvel objet
	 */
	public void fouille() {
		Image objet =null;
		Random alea = new Random();
		int tot = alea.nextInt(100);
		if(getNbAction() <= 0) return;
		
		setNbAction(getNbAction()-1);
		
		if(tot <= 10) {
			Bouclier b = new Bouclier();
			ajoutObj(b);
			inventaire.get(inventaire.indexOf(b)).execute();
			objet = new Image("file:res/bouclier.png",125,125, false,true);
			
		}
		else if(tot > 10 && tot <= 35) {
			Boue boue = new Boue();
			ajoutObj(boue);
			inventaire.get(inventaire.indexOf(boue)).execute();
			objet = new Image("file:res/boue.png",125,125, false,true);

		}
		else if(tot > 35 && tot <= 60) {
			CafeM caf = new CafeM();
			ajoutObj(caf);
			inventaire.get(inventaire.indexOf(caf)).execute();
			objet = new Image("file:res/cafe.png",125,125, false,true);

		}
		else if(tot < 60 && tot >= 75) {
			Chaussette c = new Chaussette();
			ajoutObj(c);
			inventaire.get(inventaire.indexOf(c)).execute();
			objet = new Image("file:res/chaussettes.png",125,125, false,true);

		}
		else {
			Chaussure chaus = new Chaussure();
			ajoutObj(chaus);
			inventaire.get(inventaire.indexOf(chaus)).execute();
			objet = new Image("file:res/chaussures.png",125,125, false,true);

		}
		objetFouille=objet;
	}
	
	/**
	 * Active les objets de l'inventaire qui n'ont pas expiré et retire ceux qui ont expiré
	 */
	public void activeObj() {
		for(int i = 0; i < inventaire.size(); i++) {
			if(inventaire.get(i).expire())
				retireObj(inventaire.get(i));
			else 
				inventaire.get(i).execute();
		}
	}
	
	/**
	 * Affiche le plateau du monstre
	 */
	public void affichage(GraphicsContext plateaugc,double width, double heigth) {
		for(int i = 0; i < plateau[0].length; i++) {
			for(int j = 0; j < plateau.length; j++) {
				Image herbe = new Image("file:"+Terrain.HERBE.toString(), width/plateau.length+1,heigth/plateau[0].length+1, false,true);
				plateaugc.drawImage(herbe, j*width/plateau.length, i*heigth/plateau[0].length);
				if (getPosition().x == j && getPosition().y == i) {
					Image Img = new Image("file:"+Terrain.MONSTRE.toString(), width/plateau.length+1,heigth/plateau[0].length+1, false,true);
					plateaugc.drawImage(Img, j*width/plateau.length, i*heigth/plateau[0].length);
				}
				else if(plateau[j][i].estPasse()) {
					Image Img = new Image("file:"+Terrain.TRACE.toString(), width/plateau.length+1,heigth/plateau[0].length+1, false,true);
					plateaugc.drawImage(Img, j*width/plateau.length, i*heigth/plateau[0].length);
				}
				if(plateau[j][i].isEstDecouverte() || plateau[j][i].isEstMalDecouverte()) {
					Image Img = new Image("file:"+Terrain.DANGER.toString(), width/plateau.length+1,heigth/plateau[0].length+1, false,true);
					plateaugc.drawImage(Img, j*width/plateau.length, i*heigth/plateau[0].length);
				}
			}
		}
	}
	
	/**
	 * Déplace le monstre dans le plateau selon une direction
	 * @param direction Direction
	 * @return boolean
	 */
	
	public boolean deplacement(Direction direction) {
		if(direction == null) return false;
		if(!deincrAction()) return false;
		Position newPosition = new Position (getPosition().x + direction.getDirection().x,getPosition().y + direction.getDirection().y);
		if(newPosition.x < 0 || newPosition.y < 0 || newPosition.x >= getLargeur() || newPosition.y >= getLongeur()) return false;
		if(!plateau[newPosition.x][newPosition.y].estPasse()) {
			plateau[newPosition.x][newPosition.y].passage(getNbTour());
			this.setPosition(newPosition);
			return true;
		}
		return false;
	}
	/**
	 * Converti en une direction en String
	 * @param s String
	 * @return Direction
	 */
	public Direction convertir(String s) {
		switch(s) {
		case "\u2199":
			return Direction.BAS_GAUCHE;
		case "\u2193":
			return Direction.BAS;
		case "\u2198":
			return Direction.BAS_DROITE;
		case "\u2190":
			return Direction.GAUCHE;
		case "\u2192":
			return Direction.DROITE;
		case "\u2196":
			return Direction.HAUT_GAUCHE;
		case "\u2191":
			return Direction.HAUT;
		case "\u2197":
			return Direction.HAUT_DROITE;
		case "F":
			fouille();
			return null;
		default:
			return null;
		}
	}
	/**
	 * Permet d'avoir l'image du dernier objet fouiller
	 * @return Image
	 */
	public static Image getObjetFouille() {
		return objetFouille;
	}
	/**Permet de savoir si le monstre a gagné
	 * 
	 * @return boolean
	 */
	public boolean hasWin() {
		for(int i = 0; i < getLongeur(); i++) {
			for(int j = 0; j < getLargeur(); j++) {
				if (!plateau[j][i].estPasse()) return false;
			}
		}
		return true;
	}
	/**Permet de savoir si le monstre a perdu
	 * 
	 * @return boolean
	 */
	public boolean hasLose() {
		for(int i = getPosition().y - 1; i <= getPosition().y + 1; i++) {
			for(int j = getPosition().x - 1 ; j <= getPosition().x + 1; j++) {
				if(i >= 0 && j >= 0 && i < getLongeur() && j < getLargeur()) {
					if (!plateau[j][i].estPasse()) return false;
				}
			}
		}
		return true;
	}
	/**Permet d'obtenir une direction aléatoire
	 * 
	 * @return String
	 */
	public String directionAleatoire() {
		return "" + aleatoire.nextInt(10);
	}
	
	/**
	 * 
	 * @param x int
	 * @param y int
	 * @return boolean
	 */
	public boolean estPasse(int x, int y) {
		return plateau[x][y].estPasse();
	}
}
