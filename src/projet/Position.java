package projet;
/**Cette classe permet de gérer la position sur un plateau
 * 
 * @author guyardc roelandg verdierr boucheza
 *
 */
public class Position {
	int x;
	int y;
	/**Constructeur
	 * 
	 * @param x int
	 * @param y int
	 */
	Position(int x,int y){
		this.x = x;
		this.y = y;
	}
	/**Converti la position en String
	 * @return String
	 */
	@Override
	public String toString() {
		return "Position [x=" + x + ", y=" + y + "]";
	}
	/**
	 *  @return int
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}
	/**Vérifie si la position entrée est égale à la position courante
	 * @param obj Object
	 * @return boolean
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Position other = (Position) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}
	
}