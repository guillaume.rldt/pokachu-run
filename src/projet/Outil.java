package projet;

import java.util.Scanner;
/**
 * Classe qui regroupe les differentes fonctions utiles d'affichage et de conversion
 * @author guyardc
 *
 */
public class Outil {
	/**
	 * Permet de lire la saisie clavier 
	 * @return String
	 */
	public static String readString() {
		String res = "";
		Scanner scanner = new Scanner(System. in);
		res = scanner. nextLine();
		scanner.close();
		return res;
	}

	/**
	 * permet de lire la saisie clavier et d'ecrire un message
	 * @param message String
	 * @return String
	 */
	public static String readString(String message) {
		String res = "";
		Scanner scanner = new Scanner(System. in);
		System.out.println(message);
		res = scanner. nextLine();
		scanner.close();
		return res;
	}

	/**
	 * permet de nettoyer l'ecran
	 */
	public static void clearScreen() {
		for( int i = 0 ; i < 100; i++) {
			System.out.println();
		}
	}
}
