package projet;

import java.util.Random;

import javafx.scene.canvas.GraphicsContext;

/**
 * Permet d'avoir la base d'un plateau qui sera modifié selon si la personne est monstre ou chasseur
 * 
 * @author guyardc roelandg verdierr boucheza
 *
 */
public abstract class Plateau {
	//attributs
	public static Case [][] plateau;
	protected static int nbTour;
	static Position position;
	private final static int TAILLE = 8;
	private static boolean jeuTerminer = false;
	protected static Random aleatoire = new Random();
	//constructeurs
	/**
	 * Crée un plateau de taille TAILLE
	 */
	public Plateau() {
		this(TAILLE, TAILLE);
	}
	
	/**
	 * Crée un plateau de taille nbLig x nbCol
	 * @param nbLig int
	 * @param nbCol int
	 */
	public Plateau(int nbLig, int nbCol) {
		plateau = new Case [nbLig][nbCol];
		for(int i = 0; i < plateau.length; i++) {
			for(int j = 0; j < plateau[0].length; j++) {
				plateau[i][j] = new Case ();
			}
		}
		nbTour = 0;
	}
	
	/**
	 * Crée un plateau carré de taille x
	 * @param x int
	 */
	public Plateau(int x) {
		this(x, x);
	}
	/**
	 * Crée un plateau grace un tableau de case
	 * @param newPlateau Case [][]
	 */
	public Plateau(Case [][] newPlateau) {
		plateau = newPlateau;
		}
	
	/**
	 * Permet d'initialiser la possition du joueur
	 */
	public void initPosition() {
		int x = aleatoire.nextInt(plateau.length);
		int y = aleatoire.nextInt(plateau[0].length);
		position = new Position(x,y);
		plateau[x][y].passage(nbTour);
	}

	/**
	 * Incrémentation du nombre de tour
	 */
	public static void tourSuivant() {
		nbTour++;
	}
	/**
	 * Permet d'acceder à la largeur du plateau
	 * @return int
	 */
	public static int getLargeur () {
		return plateau.length;
	}
	/**
	 * Permet d'acceder à la longueur du plateau
	 * @return int
	 */
	public static int getLongeur () {
		return plateau[0].length;
	}
	/**
	 * permet d'aceder au nombre de tour
	 * @return int
	 */
	public static int getNbTour() {
		return nbTour;
	}
	/**
	 * permet d'aceder à la position
	 * @return Position
	 */
	public Position getPosition() {
		return position;
	}
	/**
	 * permet de changer la position
	 * @param position Position
	 */
	public void setPosition(Position position) {
		Plateau.position = position;
	}
	/**
	 * 
	 * @param plateaugc GraphicsContext
	 * @param width double
	 * @param heigth double
	 */
	public abstract void affichage(GraphicsContext plateaugc,double width, double heigth); 
	/**
	 * Permet de savoir si le jeu est terminé
	 * @return boolean
	 */
	public static boolean isJeuTerminer() {
		return jeuTerminer;
	}
	/**
	 * permet de rendre le jeu terminé
	 */
	public static void jeuTerminer() {
		setJeuTerminer(true);
	}
	/**
	 * permet d'aceder au plateau
	 * @return Case[][]
	 */
	public static Case[][] getPlateau() {
		return plateau;
	}
	/**
	 * permet de changer la valeur de jeuTerminer
	 * @param jeuTerminer
	 */
	public static void setJeuTerminer(boolean jeuTerminer) {
		Plateau.jeuTerminer = jeuTerminer;
	}	
	
}
