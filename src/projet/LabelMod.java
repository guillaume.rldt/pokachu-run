package projet;

import javafx.geometry.Pos;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class LabelMod extends StackPane {
	public int width;
	public int heigth;
	public Text text;
	public Rectangle rt;
	LabelMod(String name) {
		this(name,250,50);
	}
	LabelMod(String name,int width,int heigth) {
		text = new Text(name);
		text.setFont(Font.font(20));
		text.setFill(Color.WHITE);
		
		rt = new Rectangle(width, heigth);
		rt.setOpacity(0.5);
		rt.setFill(Color.BLACK);
		setAlignment(Pos.CENTER);
		getChildren().addAll(rt, text);
	}
	public void setText(Text text) {
		this.text = text;
		text.setFont(Font.font(20));
		text.setFill(Color.WHITE);
	}
}