package projet;

/**
 * Cette enum permet de mettre des texture sur les case du plateau
 * @author guyardc roelandg boucheza verdierr
 *
 */
public enum Terrain {
	HERBE("res/herbe.png"), 
	DANGER("res/danger.png"),
	MONSTRE("res/monstre.png"),
	TRACE("res/trace.png"),
	TIR("res/tir.png");
	//attributs
	private final String url;

	//constructeur
	/**
	 * 
	 * @param url String
	 */
	private Terrain(final String url) {
		this.url = url;
	}

	/**
	 * Permet d'avoir l'url d'une image
	 * @return String
	 */
	public String toString() {
        return url;
    }
}
