package projet;

/**
 * Permet de creer les differents direction possible dans le jeu 
 * @author guyardc roelandg verdierr boucheza
 *
 */
public enum Direction {
	/** egal a la position 0,-1 **/HAUT(new Position(0,-1)),
	/** egal a la position 1,-1 **/HAUT_DROITE(new Position(+1,-1)),
	/** egal a la position 1,0 **/DROITE(new Position(+1,0)), 
	/** egal a la position 1,1 **/BAS_DROITE(new Position(+1,+1)),
	/** egal a la position 0,1 **/BAS(new Position(0,+1)),
	/** egal a la position -1,1 **/BAS_GAUCHE(new Position(-1,+1)), 
	/** egal a la position -1,0 **/GAUCHE(new Position(-1,0)),
	/** egal a la position -1,1 **/HAUT_GAUCHE(new Position(-1,-1));
	//attributs
	private Position direction;
	//constructeur
	/**
	 * @param p Position
	 */
	private Direction (Position p) {
		direction = new Position(p.x, p.y);
	}
	/** 
	 * Retourner la valeur de la direction
	 * @return Position
	 */
	public Position getDirection() {
		return this.direction;
	}
}
