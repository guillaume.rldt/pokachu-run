package projet;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.*;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.*;

/**
 * Permet de lancer une partie
 * @author guyardc roelandg verdierr boucheza
 *
 */
public class run extends Application {
	//attributs
	private static int nbLig = 10;
	private static int nbCol = 10;
	static PlateauChasseur chasseur;
	static PlateauMonstre monstre;
	static Random random = new Random();
	/**
	 * Permet de choisir un mode de jeu et de lancer la methode associÃ©
	 * 
	 * @param args String[]
	 */
	public static void main(String[] args) {
		Application.launch(args);
	}
	/**
	 * Permet de lancer une partie entre 2 joueurs
	 * 
	 */
	public static void deroulementJcJ() {
		chasseur = new PlateauChasseur(nbLig,nbCol);
		monstre = new PlateauMonstre (nbLig,nbCol);
		monstre.initPosition();
		Plateau.tourSuivant();
		monstre.affichage(plateaugcM,plateauM.getWidth(),plateauM.getHeight());
		actionM.setText("Action : "+PlateauMonstre.getNbAction());
		actionC.setText("Action : "+PlateauChasseur.getNbAction());

		paveDeplacement.setOnMouseClicked(event -> {
			actionM.setText("Action : "+PlateauMonstre.getNbAction());
			if (monstre.hasWin() == true) {
				quiAsGagne.setText("Pokachu s'est échappé et la team Croquette s'envoloe vers d'autres cieux!");
				imageEnd.setImage(new Image("file:res/MVictoire.gif", 400, 400, false,true));
				stage.setScene(endPage);
			}
			else if (monstre.hasLose() == true) {
				quiAsGagne.setText("Pokatchu s'est fait attrapé par la team Croquette !");
				imageEnd.setImage(new Image("file:res/MDefaite.gif", 400, 400, false,true));
				stage.setScene(endPage);
			}
		});

		tourSuivantC.setOnMouseClicked(event -> {
			stage.setScene(playPageMonstre);
			monstre.affichage(plateaugcM,plateauM.getWidth(),plateauM.getHeight());
			chasseur.rechargeAction();
			actionC.setText("Action : "+PlateauChasseur.getNbAction());
			tourC.setText("Tour : "+Plateau.getNbTour());
			effaceInventaireC();
			Plateau.tourSuivant();
		});

		tourSuivantM.setOnMouseClicked(event -> {
			monstre.activeObj();
			stage.setScene(playPageChasseur);
			chasseur.affichage(plateaugcC,plateauM.getWidth(),plateauM.getHeight());
			monstre.rechargeAction();
			actionM.setText("Action : "+PlateauMonstre.getNbAction());
			tourM.setText("Tour : "+Plateau.getNbTour());
			effaceInventaireM();
		});

		touchScreen.setOnMousePressed(event-> {
			chasseur.tir(new Position(
					(int) (((event.getX()-12)-((event.getX()-12)%(plateauC.getHeight()/nbLig)))/(plateauC.getHeight()/nbLig)),
					(int) (((event.getY()-26)-((event.getY()-26)%(plateauC.getWidth() /nbCol)))/(plateauC.getWidth() /nbCol))
					));
			chasseur.affichage(plateaugcC,plateauM.getWidth(),plateauM.getHeight());
			actionC.setText("Action : "+PlateauChasseur.getNbAction());
			if (chasseur.hasWin() == true) {
				quiAsGagne.setText("Pokatchu s'est fait attrapé par la team Croquette !");
				imageEnd.setImage(new Image("file:res/CVictoire.gif", 400, 400, true,true));
				stage.setScene(endPage);
			}
		});

		fouilleC.setOnMouseClicked(event -> {
			chasseur.fouille();
			afficheInventaireC();
			actionC.setText("Action : "+PlateauChasseur.getNbAction());
		});
	}
	static int phase = 1;
	static int sousPhase = 1;
	static boolean finPhase = false;
	/**
	 * Permet de lancer une partie joueur chasseur et IA monstre
	 * 
	 */
	public static void deroulementJcEMonstreIA() {
		chasseur = new PlateauChasseur(nbLig,nbCol);
		monstre = new PlateauMonstre (nbLig,nbCol);
		monstre.initPosition();
		Plateau.tourSuivant();
		chasseur.affichage(plateaugcC,plateauC.getWidth(),plateauC.getHeight());
		actionC.setText("Action : "+PlateauChasseur.getNbAction());

		tourSuivantC.setOnMouseClicked(event -> {
			stage.setScene(playPageChasseur);
			chasseur.affichage(plateaugcC,plateauC.getWidth(),plateauC.getHeight());
			chasseur.rechargeAction();
			actionC.setText("Action : "+PlateauChasseur.getNbAction());
			tourC.setText("Tour : "+Plateau.getNbTour());
			Plateau.tourSuivant();
			tourMonstre();

		});

		touchScreen.setOnMousePressed(event-> {
			chasseur.tir(new Position(
					(int) (((event.getX()-12)-((event.getX()-12)%(plateauC.getHeight()/nbLig)))/(plateauC.getHeight()/nbLig)),
					(int) (((event.getY()-26)-((event.getY()-26)%(plateauC.getWidth() /nbCol)))/(plateauC.getWidth() /nbCol))
					));
			chasseur.affichage(plateaugcC,plateauC.getWidth(),plateauC.getHeight());
			actionC.setText("Action : "+PlateauChasseur.getNbAction());
			if (chasseur.hasWin() == true) {
				stage.setScene(endPage);
			}
		});

		fouilleC.setOnMouseClicked(event -> {
			chasseur.fouille();
			actionC.setText("Action : "+PlateauChasseur.getNbAction());
		});
	}

	/**
	 * Permet de lancer une partie joueur monstre et IA chasseur
	 * 
	 */
	public static void deroulementJcEChasseurIA() {
		chasseur = new PlateauChasseur(nbLig,nbCol);
		monstre = new PlateauMonstre (nbLig,nbCol);
		monstre.initPosition();
		Plateau.tourSuivant();
		monstre.affichage(plateaugcM,plateauM.getWidth(),plateauM.getHeight());

		tourSuivantM.setOnMouseClicked(event -> {
			stage.setScene(playPageMonstre);
			tourChasseur();
			monstre.rechargeAction();
			actionM.setText("Action : "+PlateauMonstre.getNbAction());
			monstre.affichage(plateaugcM,plateauM.getWidth(),plateauM.getHeight());
			Plateau.tourSuivant();
		});	
	}
	/**
	 * Permet de lancer une partie avec 2 IA
	 * 
	 */
	public static void deroulementEcE() {
		chasseur = new PlateauChasseur(nbLig,nbCol);
		monstre = new PlateauMonstre (nbLig,nbCol);
		monstre.initPosition();
		monstre.affichage(plateaugcM,plateauM.getWidth(),plateauM.getHeight());
		Plateau.tourSuivant();
		while(!Plateau.isJeuTerminer()) {
			stage.setScene(playPageMonstre);
			monstre.affichage(plateaugcM,plateauM.getWidth(),plateauM.getHeight());
			tourMonstre();
			try {
				TimeUnit.SECONDS.sleep(3);
			} catch (InterruptedException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
			monstre.affichage(plateaugcM,plateauM.getWidth(),plateauM.getHeight());
			try {
				TimeUnit.SECONDS.sleep(3);
			} catch (InterruptedException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}

			stage.setScene(playPageChasseur);
			chasseur.affichage(plateaugcC,plateauC.getWidth(),plateauC.getHeight());
			tourChasseur();
			try {
				TimeUnit.SECONDS.sleep(3);
			} catch (InterruptedException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
			chasseur.affichage(plateaugcC,plateauC.getWidth(),plateauC.getHeight());
			try {
				TimeUnit.SECONDS.sleep(3);
			} catch (InterruptedException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
			Plateau.tourSuivant();
		}
	}
	
	/**
	 * Permet de faire jouer le monstre par une IA pendant un tour
	 */
	public static void tourMonstre() {
		if(phase == 1) {
			if(monstre.deplacement(monstre.convertir("6"))) {
				finPhase = false;
			} else {
				finPhase = true;
			}
		}
		if(phase == 2) {
			if(monstre.deplacement(monstre.convertir("2"))) {
				finPhase = false;
			} else {
				finPhase = true;
			}
		}
		if(phase == 3) {
			if(monstre.getPosition().x >= 3) {
				if(sousPhase == 1) {
					monstre.deplacement(Direction.GAUCHE);
					sousPhase++;
				}
				else if(sousPhase == 2) {
					if(monstre.getPosition().y < 9) {
						if(!monstre.estPasse(monstre.getPosition().x, monstre.getPosition().y+1)) {
							monstre.deplacement(Direction.BAS);
							finPhase = true;
							sousPhase = 1;
						}
					} else {
						monstre.deplacement(Direction.HAUT);
						sousPhase++;
					}
				}
				else if(sousPhase == 3) {
					monstre.deplacement(Direction.GAUCHE);
					sousPhase++;
				}
				else if(sousPhase == 4) {
					monstre.deplacement(Direction.BAS);
					sousPhase = 1;
				}
			} else {
				if(monstre.deplacement(monstre.convertir("4"))) {
					finPhase = false;
				} else {
					finPhase = true;
				}
			}
		}
		if(phase == 4) {
			if(monstre.getPosition().y == 0) {
				phase = 6;
			}
			else if(monstre.deplacement(monstre.convertir("2"))) {}
			else if(monstre.deplacement(monstre.convertir("6"))) {}
			else if(monstre.deplacement(monstre.convertir("8"))) {}		
			else if(monstre.getPosition().y <= 2) {
				phase = 6;
			}
			else {phase = 5;}
		}
		if(phase == 5) {
			if(monstre.getPosition().y == 0) {
				phase = 6;
			}
			else if(monstre.deplacement(monstre.convertir("2"))) {}
			else if(monstre.deplacement(monstre.convertir("4"))) {}
			else if(monstre.deplacement(monstre.convertir("8"))) {}		
			else if(monstre.getPosition().y <= 2) {
				phase = 6;
			}
			else {phase = 4;}
		}

		if(phase==6) {
			if(monstre.deplacement(monstre.convertir("2"))) {}
			else if(monstre.deplacement(monstre.convertir("6"))) {}
			else if(monstre.deplacement(monstre.convertir("4"))) {}	
			else if(monstre.deplacement(monstre.convertir(monstre.directionAleatoire()))) {}
			else {phase = 7;}
		}

		if(phase==7) {
			if(monstre.deplacement(monstre.convertir(monstre.directionAleatoire()))) {}
			else {phase = 6;}
		}
		if(finPhase) {
			phase++;
			finPhase = false;
		}
		monstre.rechargeAction();
	}

	/**
	 * Permet de faire jouer le chasseur par une IA pendant un tour
	 */
	public static void tourChasseur() {
		boolean trace = false;
		int alea;
		int followNb = 0;
		boolean follow = true;
		String chasseurX = "";
		String chasseurY = "";
		Position positionC = new Position(0,0);
		if(trace==false) {
			chasseurX = chasseur.tirAleatoireX();
			chasseurY = chasseur.tirAleatoireY();

			positionC = chasseur.convertir(chasseurX, chasseurY);

			chasseur.tir(positionC);

			if(chasseur.estDecouverte(positionC)) {
				trace = true;
			}				

		} else {

			positionC = chasseur.convertir(chasseurX, chasseurY);

			while(follow) {

				alea = random.nextInt(7);

				if(alea==0 && positionC.x > 0 && positionC.y > 0 ) {
					positionC.x = positionC.x - 1;
					positionC.y = positionC.y - 1;
					follow = false;
					followNb++;
				} else if(alea==1 && positionC.x < Plateau.getLargeur()-1 && positionC.y > 0 ) {
					positionC.x = positionC.x + 1;
					positionC.y = positionC.y - 1;
					follow = false;
					followNb++;
				} else if(alea==2 && positionC.x < Plateau.getLargeur()-1 && positionC.y < Plateau.getLongeur()-1 ) {
					positionC.x = positionC.x + 1;
					positionC.y = positionC.y + 1;
					follow = false;
					followNb++;
				} else if(alea==3 && positionC.x < Plateau.getLargeur()-1 ) {
					positionC.x = positionC.x + 1;
					follow = false;
					followNb++;
				} else if(alea==4 && positionC.y < Plateau.getLongeur()-1 ) {
					positionC.y = positionC.y + 1;
					follow = false;
					followNb++;
				} else if(alea==5 && positionC.x > 0 ) {
					positionC.x = positionC.x - 1;
					follow = false;
					followNb++;
				} else if(alea==6 && positionC.y > 0 ) {
					positionC.y = positionC.y - 1;
					follow = false;
					followNb++;
				} else if(followNb > 10) {
					follow = false;
					followNb = 0;
					chasseurX = chasseur.tirAleatoireX();
					chasseurY = chasseur.tirAleatoireY();
					positionC = chasseur.convertir(chasseurX, chasseurY);
				}

			}

			follow = true;

			chasseur.tir(positionC);

			if(!chasseur.estDecouverte(positionC)) {
				trace = false;;
			}

		}
		chasseur.rechargeAction();
	}

	//attributs
	static ButtonMod fouilleC, tourSuivantM, tourSuivantC;
	static Scene playPageMonstre, playPageChasseur, endPage;
	static VBox touchScreen = new VBox();
	static Label actionM, actionC, tourM, tourC, aideM, aideC, quiAsGagne;
	static VBox paveDeplacement;
	static ImageView imageEnd;
	
	static Canvas plateauM = new Canvas (500 , 520), plateauC = new Canvas (500 , 520);
	static GraphicsContext plateaugcM = plateauM.getGraphicsContext2D(), plateaugcC = plateauC.getGraphicsContext2D();
	static Canvas inventaireM = new Canvas (125 , 125), inventaireC = new Canvas (125 , 125);
	static GraphicsContext inventairegcM = inventaireM.getGraphicsContext2D(), inventairegcC = inventaireC.getGraphicsContext2D();

	static Stage stage;
	
	/**
	 * 
	 */
	public void start(Stage stagea) {
		stage = stagea;

		//creation et initialisation de homePage
		Titre titre = new Titre("Pokatchu run");
		titre.setPadding(new Insets(20, 0, 0, 0));

		ButtonMod jouer = new ButtonMod("Jouer");
		jouer.setPadding(new Insets(20, 0, 0, 0));

		ButtonMod regle = new ButtonMod("Règle");
		regle.setPadding(new Insets(20, 0, 0, 0));

		ButtonMod quitter = new ButtonMod("Quitter");
		quitter.setPadding(new Insets(20, 0, 0, 0));


		VBox vboxHomePage = new VBox();	
		vboxHomePage.setAlignment(Pos.CENTER);
		vboxHomePage.setPadding(new Insets(50, 0, 0, 0));

		vboxHomePage.getChildren().addAll(jouer, regle, quitter);

		VBox vboxHomePage2 = new VBox();	
		vboxHomePage2.setPadding(new Insets(50, 0, 0, 0));
		vboxHomePage2.setStyle("-fx-background-color: white");

		vboxHomePage2.getChildren().addAll(titre,vboxHomePage);

		Scene homePage = new Scene(vboxHomePage2,725,600);

		//creation et initialisation de rulePage
		ButtonMod retour = new ButtonMod("Retour");
		retour.setPadding(new Insets(20, 0, 0, 0));

		Label regleText = new Label(("La team Croquette essaye encore d'attraper pokachu ! \n"
				+ " Pokachu essaye de s'echapper: la team Croquette tente de le trouver, lui qui est caché dans les hautes herbes. \n"
				+ " Pour s'enfuir, pokatchu doit parcourrir toute les cases du plateau, mais ne peut pas revenir sur ses traces !\n"
				+ " La team croquette, elle, doit lancer des pokeballs sur le terrain, tout en ingnorant ou est Pokatchu. \n"
				+ "Si elle lance une pokeball la où est déja passé pikachu, il saura alors quand. \n"

				+ " \n DEROULEMENT D'UNE PARTIE \n " 
				+ " Le jeu se joue au tour par tour, le premier joueur incarne Pokachu et le deuxieme la Team Croquette\n"
				+" Pendant un tour, vous pouvez choisir de vous déplacer (pavé pour le j1 et clic sur le terrain pour le j2),  \n"
				+ " Ou de fouiller, ce qui vous donnera un objet utilisé instentanément. Chacun de sesz choix coûte une action.\n"
				+ "Chaque joueur ne dispose au départ que d'une action par tour, puis une autre rajoutée tout les cinqs tours.\n"
				
				+ " Des objets peuvent également vous donner des actions, ou vous en faire perdre. \n"
				+ " \n LISTE OBJETS POKACHU \n " 
				+ " Café: vous vous sentez en pleine forme ! donne 1 PA \n"
				+ " Chaussure: Vous etes plus rapide que jamais ! donne 2 PA \n"
				+ " Metamorph: Pendant ce tour, metamorph prends votre place si vous vous faites capturer par la TC !\n"
				+ " Ronflex: Ronflex bloque le chamin! Fais tomber vos PA à 0\n"
				+ " Chaussette: Vous trouvez de vieilles chaussettes... Il ne se passe rien ! \n "
				+ "\n LISTE OBJET TEAM CROQUETTE\n"
				+ "Café: vous vous sentez en pleine forme ! donne 1 PA \n"
				+ "SuperBall: vous avez plus de chance de capturer un pokemon! donne 2 PA \n"
				+ "MasterBall: vos chances de capturer un pokemon augment! votre prochain tir couvrira une zone plus large. \n"
				+ "Binouze: Rien de vaut une petite binouze... Mais vous en abusez beaucoup trop \n"
				+ "            et vous oublierais a partir du prochain tour où vous avez tiré. Retenez bien vos marques! \n"
				+ "Ronflex: Ronflex vous barre la route! Fais tomber vos PA à 0 \n"
				));
		regleText.setPadding(new Insets(10, 0, 0, 10));

		VBox vboxRulePage = new VBox(3);	
		vboxRulePage.setPadding(new Insets(20, 0, 0, 0));
		vboxRulePage.setStyle("-fx-background-color: white");
		vboxRulePage.getChildren().addAll(new Titre("Règles"),regleText,retour);

		Scene rulePage = new Scene(vboxRulePage,725,600);

		// creation et initialisation de playPage 
		ButtonMod jcj = new ButtonMod("Joueur contre Joueur");
		jcj.setPadding(new Insets(20, 0, 0, 0));

		ButtonMod Mbot = new ButtonMod("Monstre ordinateur");
		Mbot.setPadding(new Insets(20, 0, 0, 0));

		ButtonMod Cbot = new ButtonMod("Chasseur ordinateur");
		Cbot.setPadding(new Insets(20, 0, 0, 0));

		ButtonMod ece = new ButtonMod("Ordinateur contre ordinateur");
		ece.setPadding(new Insets(20, 0, 0, 0));

		ButtonMod parametre = new ButtonMod("Parametre de la partie");
		parametre.setPadding(new Insets(20, 0, 0, 0));

		ButtonMod retour2 = new ButtonMod("Retour");
		retour2.setPadding(new Insets(20, 0, 0, 0));

		VBox vboxPlayPage = new VBox(6);
		vboxPlayPage.setPadding(new Insets(50, 0, 0, 0));
		vboxPlayPage.setStyle("-fx-background-color: white");

		vboxPlayPage.getChildren().addAll(new Titre("Jouer"),jcj,Mbot,Cbot,ece,parametre,retour2);

		Scene selectPlayPage = new Scene(vboxPlayPage,725,600);

		// creation et initialisation de parameterPage 
		ButtonMod retour3 = new ButtonMod("Retour",130,50);
		retour3.setPadding(new Insets(20, 0, 0, 0));

		ButtonMod valider = new ButtonMod("Valider",130,50);
		valider.setPadding(new Insets(20, 0, 0, 0));

		HBox HboxParameterPage = new HBox(6);	
		HboxParameterPage.setPadding(new Insets(50, 0, 0, 0));
		HboxParameterPage.setStyle("-fx-background-color: white");
		HboxParameterPage.setAlignment(Pos.CENTER);
		HboxParameterPage.getChildren().addAll(valider,retour3);

		TextField largeur = new TextField(""+nbCol);

		Label labelLargeur = new Label("Largeur : ");
		labelLargeur.setStyle(" -fx-font: 20px Roboto;"); 
		labelLargeur.setPadding(new Insets(0, 0, 10, 0));

		HBox HboxParameterPageLargeur = new HBox(6);	
		HboxParameterPageLargeur.setPadding(new Insets(50, 0, 0, 0));
		HboxParameterPageLargeur.setStyle("-fx-background-color: white");
		HboxParameterPageLargeur.setAlignment(Pos.CENTER);
		HboxParameterPageLargeur.getChildren().addAll(labelLargeur,largeur);

		TextField longeur = new TextField(""+nbLig);

		Label labelLongeur = new Label("Longeur : ");
		labelLongeur.setStyle(" -fx-font: 20px Roboto;"); 
		labelLongeur.setPadding(new Insets(0, 0, 10, 0));

		HBox HboxParameterPageLongeur = new HBox(6);	
		HboxParameterPageLongeur.setPadding(new Insets(50, 0, 0, 0));
		HboxParameterPageLongeur.setStyle("-fx-background-color: white");
		HboxParameterPageLongeur.setAlignment(Pos.CENTER);
		HboxParameterPageLongeur.getChildren().addAll(labelLongeur,longeur);

		Label erreurParametre = new Label("");
		erreurParametre.setStyle(" -fx-font: 20px Roboto;"); 
		erreurParametre.setTextFill(Color.RED);

		VBox vboxParameterPage = new VBox(6);	
		vboxParameterPage.setPadding(new Insets(50, 0, 0, 0));
		vboxParameterPage.setStyle("-fx-background-color: white");
		vboxParameterPage.getChildren().addAll(new Titre("Paramètre"),HboxParameterPageLargeur,HboxParameterPageLongeur,erreurParametre,HboxParameterPage);
		vboxParameterPage.setAlignment(Pos.CENTER);

		Scene pageParametre = new Scene(vboxParameterPage,725,600);

		// creation et initialisation de playPageMonstre
		BorderPane borderPaneM = new BorderPane();
		borderPaneM.setCenter(plateauM);

		HBox topBorderM = new HBox();
		topBorderM.setPadding(new Insets(20, 20, 20, 30));
		borderPaneM.setTop(topBorderM);

		VBox BorderRightM = new VBox();
		BorderRightM.setPadding(new Insets(15, 20, 20, 20));

		tourM = new Label("Tour : "+PlateauMonstre.getNbTour());
		tourM.setStyle(" -fx-font: 30px Roboto;"); 
		tourM.setPadding(new Insets(0, 0, 7, 0));

		actionM = new Label("Action : "+PlateauMonstre.getNbAction());
		actionM.setStyle(" -fx-font: 30px Roboto;"); 
		actionM.setPadding(new Insets(20, 0, 0, 0));

		HBox paveDeplacementY1 = new HBox(3);
		HBox paveDeplacementY2 = new HBox(3);
		HBox paveDeplacementY3 = new HBox(3);

		paveDeplacementY1.getChildren().addAll(new ButtonDeplacement("\u2196"),new ButtonDeplacement("\u2191"),new ButtonDeplacement("\u2197"));
		paveDeplacementY2.getChildren().addAll(new ButtonDeplacement("\u2190"),new ButtonDeplacement("F"),new ButtonDeplacement("\u2192"));
		paveDeplacementY3.getChildren().addAll(new ButtonDeplacement("\u2199"),new ButtonDeplacement("\u2193"),new ButtonDeplacement("\u2198"));

		paveDeplacement = new VBox(3);		
		paveDeplacement.getChildren().addAll(paveDeplacementY1,paveDeplacementY2,paveDeplacementY3);


		tourSuivantM = new ButtonMod("Tour Suivant",126, 50);
		tourSuivantM.setPadding(new Insets(113, 0, 0, 0));

		Pane inventaireMPane = new Pane();
		inventaireMPane.getChildren().add(inventaireM);
		inventaireMPane.setMinSize(125, 125);
		inventaireMPane.setMaxSize(125 , 125);
		inventaireMPane.setBorder(new Border(new BorderStroke(Color.BLACK,BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));

		BorderRightM.getChildren().addAll(tourM,inventaireMPane,actionM,paveDeplacement,tourSuivantM);
		borderPaneM.setRight(BorderRightM);

		aideM = new Label("Aide : ");
		VBox boxAideM = new VBox();
		boxAideM.getChildren().add(aideM);
		boxAideM.setMaxSize(730, 50);
		boxAideM.setBorder(new Border(new BorderStroke(Color.BLACK,BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));

		borderPaneM.setBottom(boxAideM);
		borderPaneM.setStyle("-fx-background-color: white");
		
		playPageMonstre = new Scene(borderPaneM,730,630);

		// creation et initialisation de playPageChasseur
		BorderPane borderPaneC = new BorderPane();

		touchScreen.getChildren().add(plateauC);
		touchScreen.setPadding(new Insets(26, 2, 0, 32));
		borderPaneC.setCenter(touchScreen);

		HBox topBorderC = new HBox(2);
		topBorderC.setPadding(new Insets(20, 20, 20, 30));
		borderPaneC.setTop(topBorderC);

		Pane inventaireCPane = new Pane();
		inventaireCPane.getChildren().add(inventaireC);
		inventaireCPane.setMinSize(125 , 125);
		inventaireCPane.setMaxSize(125 , 125);
		inventaireCPane.setBorder(new Border(new BorderStroke(Color.BLACK,BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));

		tourC = new Label("Tour : "+PlateauChasseur.getNbTour());
		tourC.setPadding(new Insets(0, 0, 5, 0));
		tourC.setStyle(" -fx-font: 30px Roboto;"); 
		tourC.setAlignment(Pos.CENTER);

		actionC = new Label("Action : "+PlateauChasseur.getNbAction());
		actionC.setPadding(new Insets(18, 0, 0, 0));
		actionC.setStyle(" -fx-font: 30px Roboto;");

		tourSuivantC = new ButtonMod("Tour Suivant",126, 50);
		tourSuivantC.setPadding(new Insets(175, 200, 0, 0));

		fouilleC = new ButtonMod("Fouille",126, 50);
		fouilleC.setPadding(new Insets(10, 200, 0, 0));

		VBox rightBorderC = new VBox(2);
		rightBorderC.setPadding(new Insets(15, 0, 20, 50));
		rightBorderC.getChildren().addAll(tourC,inventaireCPane,actionC,fouilleC,tourSuivantC);
		borderPaneC.setRight(rightBorderC);

		Label aideC = new Label("Aide : ");
		VBox boxAideC = new VBox();
		boxAideC.getChildren().add(aideC);
		boxAideC.setMaxSize(730, 50);
		boxAideC.setBorder(new Border(new BorderStroke(Color.BLACK,BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));

		borderPaneC.setBottom(boxAideC);
		borderPaneC.setStyle("-fx-background-color: white");
		
		playPageChasseur = new Scene(borderPaneC,730,630);		

		//initialisation des mouseevent de la endPage
		BorderPane borderPaneEndPage = new BorderPane();
		
		Label gameOver = new Label("Game Over");
		gameOver.setStyle(" -fx-font: 30px Roboto;"); 
		gameOver.setPadding(new Insets(20, 0, 0, 0));
		
		borderPaneEndPage.setTop(gameOver);
		BorderPane.setAlignment(gameOver, Pos.CENTER);
	    imageEnd = new ImageView(new Image("file:res/monstre.png",400,400,false,true));
	    
		quiAsGagne = new Label("");
		quiAsGagne.setStyle(" -fx-font: 20px Roboto;"); 
		quiAsGagne.setPadding(new Insets(20, 0, 0, 0));
		
		VBox centerBorderPane = new VBox();
		centerBorderPane.getChildren().addAll(imageEnd,quiAsGagne);
		centerBorderPane.setAlignment(Pos.CENTER);
		
		borderPaneEndPage.setCenter(centerBorderPane);
	
		ButtonMod recommencer = new ButtonMod("Recommencer",150,50);
		ButtonMod quitter2 = new ButtonMod("Quitter",150,50);

		HBox bottomBorderPaneEnd = new HBox(6);	
		bottomBorderPaneEnd.setPadding(new Insets(50, 0, 10, 0));
		bottomBorderPaneEnd.setStyle("-fx-background-color: white");
		bottomBorderPaneEnd.setAlignment(Pos.CENTER);
		bottomBorderPaneEnd.getChildren().addAll(recommencer,quitter2);
		
		borderPaneEndPage.setBottom(bottomBorderPaneEnd);
		
		borderPaneEndPage.setStyle("-fx-background-color: white");
		
		endPage = new Scene(borderPaneEndPage,730,630);

		//mouseevent

		//initialisation des mouseevent de la homePage
		jouer.setOnMouseClicked(event -> {
			stage.setScene(selectPlayPage);
		});
		regle.setOnMouseClicked(event -> {
			stage.setScene(rulePage);
		});
		quitter.setOnMouseClicked(event -> {
			stage.close();
		});
		retour.setOnMouseClicked(event -> {
			stage.setScene(homePage);
		});

		//initialisation des mouseevent de la rulePage
		retour2.setOnMouseClicked(event -> {
			stage.setScene(homePage);
		});

		//initialisation des mouseevent de la playPage
		jcj.setOnMouseClicked(event -> {
			stage.setScene(playPageMonstre);
			deroulementJcJ();
		});	
		Mbot.setOnMouseClicked(event -> {
			stage.setScene(playPageChasseur);
			deroulementJcEMonstreIA();
		});
		Cbot.setOnMouseClicked(event -> {
			stage.setScene(playPageMonstre);
			deroulementJcEChasseurIA();
		});
		ece.setOnMouseClicked(event -> {
			stage.setScene(playPageMonstre);
			deroulementEcE();
		});
		parametre.setOnMouseClicked(event -> {
			stage.setScene(pageParametre);
		});

		//initialisation des mouseevent de la parameterPage
		retour3.setOnMouseClicked(event -> {
			stage.setScene(selectPlayPage);
		});
		valider.setOnMouseClicked(event -> {
			try {
				if(Integer.parseInt(largeur.getText()) >= 15 && Integer.parseInt(longeur.getText()) >= 15) {
					nbCol = Integer.parseInt(largeur.getText());
					nbLig = Integer.parseInt(longeur.getText());
					stage.setScene(selectPlayPage);
				}
				else erreurParametre.setText("Erreur valeur supérieur à 15");

			}
			catch (Exception e) {
				erreurParametre.setText("Erreur valeur entrée invalide");
			}
		});

		//initialisation des mouseevent de la playPageMonstre
		inventaireM.setOnMouseEntered(event -> {
			aideM.setText("Aide : Permet de voir l'objet obtenu apres la fouille");
		});
		inventaireM.setOnMouseExited(event -> {
			aideM.setText("Aide : ");
		});
		tourM.setOnMouseEntered(event -> {
			aideM.setText("Aide : Affiche le numéro du tour actuel");
		});
		tourM.setOnMouseExited(event -> {
			aideM.setText("Aide : ");
		});
		actionM.setOnMouseEntered(event -> {
			aideM.setText("Aide : Affiche le numéro du tour actuel");
		});
		actionM.setOnMouseExited(event -> {
			aideM.setText("Aide : ");
		});
		tourSuivantM.setOnMouseEntered(event -> {
			aideM.setText("Aide : Permet de signifier que vous avez fini votre tour");
		});
		tourSuivantM.setOnMouseExited(event -> {
			aideM.setText("Aide : ");
		});
		borderPaneM.getCenter().setOnMouseEntered(event -> {
			aideM.setText("Aide : Permet de voir où vous vous trouvez et où le chasseur tire");
		});
		borderPaneM.getCenter().setOnMouseExited(event -> {
			aideM.setText("Aide : ");
		});

		//initialisation des mouseevent de la playPageChasseur
		inventaireC.setOnMouseEntered(event -> {
			aideC.setText("Aide : Permet de voir l'objet obtenu apres la fouille");
		});
		inventaireC.setOnMouseExited(event -> {
			aideC.setText("Aide : ");
		});
		tourC.setOnMouseEntered(event -> {
			aideC.setText("Aide : Affiche le numéro du tour actuel");
		});
		tourC.setOnMouseExited(event -> {
			aideC.setText("Aide : ");
		});
		tourSuivantC.setOnMouseEntered(event -> {
			aideC.setText("Aide : Permet de signifier que vous avez fini votre tour");
		});
		tourSuivantC.setOnMouseExited(event -> {
			aideC.setText("Aide : ");
		});
		touchScreen.setOnMouseEntered(event -> {
			aideC.setText("Aide : Permet de choisir la case dans laquelle vous voulez tirer");
		});
		touchScreen.setOnMouseExited(event -> {
			aideC.setText("Aide : ");
		});
		actionC.setOnMouseEntered(event -> {
			aideC.setText("Aide : Permet de fouiller pour obtenir un objet");
		});
		actionC.setOnMouseExited(event -> {
			aideC.setText("Aide : ");
		});
		fouilleC.setOnMouseEntered(event -> {
			aideM.setText("Aide : Permet de fouiller pour obtenir un objet");
		});
		fouilleC.setOnMouseExited(event -> {
			aideM.setText("Aide : ");
		});
		
		//initialisation des mouseevent de la EndPage
		quitter2.setOnMouseClicked(event -> {
			stage.close();
		});
		recommencer.setOnMouseClicked(event -> {
			Plateau.setJeuTerminer(false);
			stage.setScene(selectPlayPage);
		});


		stage.setTitle("Pokatchu run !!!");
		stage.setScene(homePage);
		stage.setResizable(false);
		stage.show();
	}

	/**
	 * Permet de créer des boutons avec un design pour le déplacement
	 * @author guyardc roelandg verdierr boucheza
	 *
	 */
	public static class ButtonDeplacement extends StackPane {
		public Text text;
		public Rectangle rt;
		ButtonDeplacement(String name) {
			text = new Text(name);
			text.setFont(Font.font(20));
			text.setFill(Color.WHITE);

			rt = new Rectangle(40, 40);
			rt.setOpacity(0.5);
			rt.setFill(Color.BLACK);
			setAlignment(Pos.CENTER);
			getChildren().addAll(rt, text);

			setOnMouseEntered(event -> {
				rt.setFill(Color.WHITE);
				text.setFill(Color.BLACK);
				if(name.equals("F")) {
					aideM.setText("Aide : Permet de fouiller pour obtenir un objet");
				}
				else aideM.setText("Aide : Permet de se deplacer");
			});

			setOnMouseExited(event -> {
				rt.setFill(Color.BLACK);
				text.setFill(Color.WHITE);
				aideM.setText("Aide : ");
			});

			setOnMouseClicked(event -> {
				monstre.deplacement(monstre.convertir(name));
				monstre.affichage(plateaugcM,plateauM.getWidth(),plateauM.getHeight());
				afficheInventaireM();
			});
		}
	}

	/**
	 * Permet de créer un Label avec un design de titre
	 * @author guyardc roelandg verdierr boucheza
	 *
	 */
	public static class Titre extends StackPane {
		Titre(String name) {
			Text text = new Text(name);
			text.setFont(Font.font(40));
			text.setFill(Color.BLACK);

			setAlignment(Pos.CENTER);
			getChildren().addAll(text);
		}
	}
	
	/**
	 * Permet d'afficher l'inventaire du monstre
	 */
	public static void afficheInventaireM() {
		effaceInventaireM();
		inventairegcM.drawImage( PlateauMonstre.getObjetFouille(), 0,0);	
	}
	
	/**
	 * Permet d'afficher l'inventaire du chasseur
	 */
	private static void afficheInventaireC() {
		effaceInventaireC();
		inventairegcC.drawImage( PlateauChasseur.getObjetFouille(), 0,0);		
	}
	
	/**
	 * Permet de supprimer l'inventaire du monstre
	 */
	public static void effaceInventaireM() {
		inventairegcM.clearRect(0, 0, inventaireM.getWidth(),inventaireM.getHeight());
	}

	/**
	 * Permet de supprimer l'inventaire du chasseur
	 */
	public static void effaceInventaireC() {
		inventairegcC.clearRect(0, 0, inventaireC.getWidth(),inventaireC.getHeight());
	}
}

