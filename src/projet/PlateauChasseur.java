package projet;

import java.util.ArrayList;
import java.util.Random;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import objet.Binouze;
import objet.CafeC;
import objet.Mitraillette;
import objet.ObjetChasseur;
import objet.Obus;
import objet.Pie;

/**
 * Creer le plateau du chasseur
 * 
 * @author guyardc roelandg verdierr boucheza
 *
 */
public class PlateauChasseur extends Plateau{
	
	//attributs
	private static Image objetFouille;
	private static int nbAction;
	@SuppressWarnings("unused")
	private Position position;
	protected ArrayList <ObjetChasseur> inventaire;
	private static boolean obusDispo=false;
	private static boolean fouilleDispo=true;

	//constructeur
	/**
	 * Crée un plateau de taille par défaut
	 */
	public PlateauChasseur() {
		super();
		nbAction=1;

		inventaire = new ArrayList <ObjetChasseur>();
	}
	
	/**
	 * Crée un plateau de taille nbLig x nbCol
	 * @param nbLig int
	 * @param nbCol int
	 */
	public PlateauChasseur(int nbLig, int nbCol) {
		super(nbLig, nbCol);
		nbAction=1;

		inventaire = new ArrayList <ObjetChasseur>();
	}
	
	/**
	 * Crée un plateau carré de taille x
	 * @param x int
	 */
	public PlateauChasseur(int x) {
		this(x,x);
	}
	
	/**
	 * Crée un plateau grace un tableau de case
	 * @param newPlateau Case [][]
	 */
	public PlateauChasseur(Case [][] newPlateau) {
		super (newPlateau);
		nbAction=1;
		inventaire = new ArrayList <ObjetChasseur>();
	}

	/**
	 * Recharge le nombre d'actions en fonction du tour
	 */
	public void rechargeAction() {
		nbAction = nbTour/3 + 1;
	}

	/**
	 * permet d'ajouter un objet dans l'inventaire
	 * @param o ObjetChasseur
	 */
	public void ajoutObj(ObjetChasseur o) {
		inventaire.add(o);
	}
	
	/**
	 * permet de retirer un objet de l'inventaire
	 * @param o ObjetChasseur
	 */
	public void retireObj(ObjetChasseur o) {
		if(o.expire())
			inventaire.remove(o);
	}
	
	/**
	 * Permet avoir le nombre d'action disponible
	 * @return int
	 */
	public static int getNbAction() {
		return nbAction;
	}
	
	/**
	 * Permet de changer la valeur de nbAction
	 * @param nbA
	 */
	public static void setNbAction(int nbA) {
		nbAction = nbA;
	}
	
	/**
	 * permet de désactiver la fouille
	 * @param fouilleDispoo boolean
	 */
	public static void setFouilleDispo(boolean fouilleDispoo) {
		fouilleDispo = fouilleDispoo;
	}
	
	/**
	 * Permet de fouiller une case et d'avoir un nouvel objet
	 */
	public void fouille() {
		Image objet=null;
		if(!fouilleDispo)return;
		if(getNbAction() <= 0) return;
		Random alea = new Random();
		int tot = alea.nextInt(100);
		if(tot <= 10) {
			Binouze b = new Binouze();
			ajoutObj(b);
			inventaire.get(inventaire.indexOf(b)).execute();
			objet = new Image("file:res/binouze.png",125,125, false,true);

		}
		else if(tot > 10 && tot <= 35) {
			CafeC c = new CafeC();
			ajoutObj(c);
			inventaire.get(inventaire.indexOf(c)).execute();
			objet = new Image("file:res/cafe.png",125,125, false,true);

		}
		else if(tot > 35 && tot <= 50) {
			Mitraillette m = new Mitraillette();
			ajoutObj(m);
			inventaire.get(inventaire.indexOf(m)).execute();
			objet = new Image("file:res/super.png",125,125, false,true);

		}
		else if(tot > 55 && tot <= 70) {
			Obus o = new Obus();
			ajoutObj(o);
			inventaire.get(inventaire.indexOf(o)).execute();
			objet = new Image("file:res/master.png",125,125, false,true);

		}
		else {
			Pie p = new Pie();
			ajoutObj(p);
			inventaire.get(inventaire.indexOf(p)).execute();
			objet = new Image("file:res/boue.png",125,125, false,true);

		}
		nbAction--;
		objetFouille=objet;

	}
	
	/**
	 * Affiche le plateau du chasseur
	 */
	public void affichage(GraphicsContext plateaugc,double width, double heigth) {
		for(int i = 0; i < plateau[0].length; i++) {
			for(int j = 0; j < plateau.length; j++) {
				Image herbe = new Image("file:"+Terrain.HERBE.toString(), width/plateau.length+1,heigth/plateau[0].length+1, false,true);
				plateaugc.drawImage(herbe, j*width/plateau.length, i*heigth/plateau[0].length);
				if (plateau[j][i].isEstMalDecouverte()) {
					Image Img = new Image("file:"+Terrain.TIR.toString(), width/plateau.length+1,heigth/plateau[0].length+1, false,true);
					plateaugc.drawImage(Img, j*width/plateau.length, i*heigth/plateau[0].length);
				}
				else if(plateau[j][i].isEstDecouverte()) {
					plateaugc.strokeText(plateau[j][i].getNbToursTrace().toString(), j*width/plateau.length+(width/plateau.length)/2, i*heigth/plateau[0].length+(heigth/plateau[0].length)/2, 300);
				}
			}
		}
	}


	/**retourne true si le chasseur a reussi à tirer et le chasseur tire et decouvre la case, false sinon
	 * 
	 * @param p Position
	 * @return boolean
	 */
	public  boolean tir(Position p) {
		position=p;
		if(p == null) return false;
		if(obusDispo) {
			tirObus(p);
			return true;
		}
		if(deincrAction()) {
			if(p.x < 0 || p.x > getLargeur() - 1) return false;
			if(p.y < 0 || p.y > getLongeur() - 1) return false;
			if(p.equals(getPosition())) {
				Plateau.jeuTerminer();
				return true;
			}
			else if(plateau[p.x][p.y].estPasse()) {
				plateau[p.x][p.y].setEstMalDecouverte(false);
				plateau[p.x][p.y].setEstDecouverte(true);
				return true;
			}
			else {
				plateau[p.x][p.y].setEstMalDecouverte(true);
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Permet d'utiliser l'objet obus si il a été obtenu
	 * @param p
	 */
	public void tirObus(Position p) {
		obusDispo=false;
		tir(new Position(p.x-1,p.y));
		tir(new Position(p.x,p.y-1));
		tir(p);
		tir(new Position(p.x+1,p.y));
		tir(new Position(p.x,p.y+1));

		fouilleDispo=true;
	}
	
	/**
	 * Permet de changer la valeur de obusDispo
	 * @param obusDispooo
	 */
	public static void setObusDispo(boolean obusDispooo) {
		obusDispo = obusDispooo;
	}

	/**
	 * Permet de convertir les entrées saisies en Position
	 * 
	 * @param s1 String
	 * @param s2 String
	 * @return Position
	 */
	public Position convertir(String s1, String s2) {
		int x;
		int y;
		if(estUnCaratere(s2)) y = s2.toUpperCase().charAt(0) - 'A';
		else return null;

		if(estUnNombre(s1)) x = Integer.parseInt(s1);
		else return null;

		return new Position(x, y);	
	}
	
	
	/**
	 * Permet de determiner si il y a bien qu'un seul caractere de saisie
	 * 
	 * @param s String
	 * @return boolean
	 */
	private boolean estUnCaratere(String s) {
		return s.length() > 0 && s.length() <=1;
	}
	/**
	 * Permet de verifier que le caractere saisie est bien un chiffre
	 * 
	 * @param s String
	 * @return boolean
	 */
	private boolean estUnNombre(String s) {
		if(s.length() > 0) {
			for(int i = 0; i < s.length(); i++) {
				if(s.charAt(i) > '9' || s.charAt(i) < '0') {
					return false;
				}
			}
			return true;
		}
		return false;
	}
	
	/**
	 * Permet de choisir aleatoirement une valeur de x
	 * 
	 * @return String
	 */
	public String tirAleatoireX() {
		return "" + aleatoire.nextInt(getLargeur());
	}
	/**
	 * Permet de choisir aleatoirement une valeur de y
	 * 
	 * @return String
	 */
	public String tirAleatoireY() {
		return "" + (char) ('A' + aleatoire.nextInt(getLongeur()));
	}
	/**
	 * incremente nbAction si c'est possible
	 * @return boolean
	 */
	public boolean deincrAction() {
		if(nbAction > 0) {
			nbAction--;
			return true;
		}
		return false;
	}
	
	/**
	 * Permet de savoir si la parti est terminé
	 * @return boolean
	 */
	public boolean hasWin() {
		return isJeuTerminer();		
	}

	/**
	 * Permet de savoir si une case est découverte
	 * @return Position
	 */
	public boolean estDecouverte(Position p) {
		return plateau[p.x][p.y].isEstDecouverte();
	}
	
	/**
	 * Permet de d'avoir l'image du dernier objet fouiller
	 * @return Image
	 */
	public static Image getObjetFouille() {
		return objetFouille;
	}
	
	/**
	 * Permet de d'accéder à l'inventaire
	 * @return ArrayList<ObjetChasseur>
	 */
	public ArrayList<ObjetChasseur> getInventaire() {
		return inventaire;
	}
	
}