package projet;

import javafx.geometry.Pos;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 * Permet de creer des boutons avec une valeur modifier
 * @author guyardc roelandg verdierr boucheza
 *
 */
public class ButtonMod extends StackPane {
	public int width;
	public int heigth;
	public Text text;
	public Rectangle rt;
	//constructeur
	/**
	 * Permet de créer un bouton avec un text mis en paramètre
	 * @param name String
	 */
	ButtonMod(String name) {
		this(name,270,50);
	}
	/**
	 * Permet de créer un bouton avec un text mis en paramètre et de taille définit
	 * @param name String
	 * @param width int
	 * @param heigth int
	 */
	ButtonMod(String name,int width,int heigth) {
		text = new Text(name);
		text.setFont(Font.font(20));
		text.setFill(Color.WHITE);
		
		rt = new Rectangle(width, heigth);
		rt.setOpacity(0.5);
		rt.setFill(Color.BLACK);
		setAlignment(Pos.CENTER);
		getChildren().addAll(rt, text);
		
		setOnMouseEntered(event -> {
			rt.setFill(Color.WHITE);
			text.setFill(Color.BLACK);
		});
		
		setOnMouseExited(event -> {
			rt.setFill(Color.BLACK);
			text.setFill(Color.WHITE);
		});
	}
}