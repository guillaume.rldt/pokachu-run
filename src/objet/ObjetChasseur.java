package objet;

/**
 * permet de differencier les ObjetChasseur des autres objets
 * @author guyardc
 *
 */

public abstract class ObjetChasseur extends Objet{
	/**
	 * constructeur en super 
	 * @param duree
	 */
	public ObjetChasseur(int duree) {
		super(duree);
	}
}


