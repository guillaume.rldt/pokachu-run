package objet;

import projet.PlateauChasseur;

/**
 * Cet objet permets au chasseur de tirer une fois de plus pendant 3 tours
 * @author guyardc
 *
 */

public class Mitraillette extends ObjetChasseur {
	private final static int DUREE = 1;
	/**
	 * constructeur super
	 */
	public Mitraillette() {
		super(DUREE);
	}
	/**
	 * execute l'objet
	 */
	public void execute() {
		PlateauChasseur.setNbAction(PlateauChasseur.getNbAction()+2);
	}
	
	

}
