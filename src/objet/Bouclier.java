package objet;

import projet.Plateau;

/**
 * Cet objet permet au monstre d'etre protege des tirs du chasseur pendant 3 tours
 * @author guyardc
 *
 */

public class Bouclier extends ObjetMonstre {
	private final static int DUREE = 1;
	/**
	 * constructeur super
	 */
	public Bouclier() {
		super(DUREE);
	}
	/**
	 * execute l'objet
	 */
	public void execute() {
		if(Plateau.isJeuTerminer())
			Plateau.setJeuTerminer(false);
	}
}
