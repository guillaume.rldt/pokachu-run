package objet;

import projet.PlateauChasseur;

/**
 * Cet objet enleve une action au chasseur pendant 1 tour
 * @author guyardc
 *
 */

public class Pie extends ObjetChasseur{
	private final static int DUREE =1;
	/**
	 * constructeur super
	 */
	public Pie() {
		super(DUREE);
		// TODO Auto-generated constructor stub
	}
	public void execute() {
	
			PlateauChasseur.setNbAction(1);
		
	}
	

	
}
