package objet;

import projet.Plateau;

/**
 * Cet objet efface toutes les traces deja decouverte par le chasseur pendant 1 tour
 * @author guyardc
 *
 */

public class Binouze extends ObjetChasseur {
	private final static int DUREE =1;
	/**
	 * constructeur super
	 */
	public Binouze( ) {
		super(DUREE);
	}
	/**
	 * execute l'objet
	 */
	public void execute() {
		int x = Plateau.getPlateau().length;
		int y = Plateau.getPlateau()[0].length;
		for (int i = 0; i < x; i++) {
			for (int j = 0; j <y; j++) {
				 Plateau.getPlateau()[i][j].setEstDecouverte(false);
				 Plateau.getPlateau()[i][j].setEstMalDecouverte(false);

				

				
			}
		}	
	}

}
