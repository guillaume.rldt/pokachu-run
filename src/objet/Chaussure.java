package objet;

import projet.PlateauMonstre;

/**
 * Cet objet prmet au monstre d'avoir une action en plus pendant 3 tours
 * @author guyardc
 *
 */

public class Chaussure extends ObjetMonstre {
	private final static int DUREE = 1;
	/**
	 * constructeur super
	 */
	public Chaussure() {
		super(DUREE);
	}
	/**
	 * execute l'objet
	 */
	public void execute() {
		PlateauMonstre.setNbAction(PlateauMonstre.getNbAction()+2);
	}
	
}
