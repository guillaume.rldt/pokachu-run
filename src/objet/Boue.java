package objet;

import projet.PlateauMonstre;

/**
 * Cet objet coince le monstre sur sa case pendant 2 tours
 * @author guyardc
 *
 */

public class Boue extends ObjetMonstre {
	private final static int DUREE = 1;
	/**
	 * constructeur super
	 */
	public Boue() {
		super(DUREE);
	}
	/**
	 * execute l'objet
	 */
	public void execute() {
		PlateauMonstre.setNbAction(0);
	}
}
