package objet;

/**
 * Cet objet ne fait rien au monstre pendant 1 tour
 * @author guyardc
 *
 */

public class Chaussette extends ObjetMonstre {
	private final static int DUREE = 1;
	/**
	 * constructeur super
	 */
	public Chaussette() {
		super(DUREE);
	}
	/**
	 * execute l'objet
	 */
	public void execute() {
		
	}

}
