package objet;

import projet.Plateau;

/**
 * permet de creer un objet 
 * @author guyardc
 *
 */

public abstract class Objet {
	protected int declenche ;
	protected int duree;
	/**
	 * Creer un objet qui dure un certain nombre de tours
	 * @param duree
	 */
	public Objet(int duree) {
		this.declenche = Plateau.getNbTour();
		this.duree = duree;
	}
	/**
	 * permet d'acceder a la valeur de declenche
	 * @return
	 */
	public int getDeclanche() {
		return declenche;
	}
/**
 * permet d'acceder a la duree
 * @return
 */
	public int getDuree() {
		return duree;
	}
/**
 * permet de savoir si un objet est expire ou pas
 * @return
 */
	public boolean expire () {
		if(Plateau.getNbTour()==(declenche+duree))return true;
		return false;
	}
	/**
	 * permettra d'executer un objet
	 */
	public abstract void execute();
}
