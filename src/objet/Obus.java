package objet;

import projet.PlateauChasseur;
import projet.PlateauMonstre;

public class Obus extends ObjetChasseur {

	private final static int CHANCE=5;
	
	public Obus() {
		super(CHANCE);
	}

	@Override
	public void execute() {
		PlateauChasseur.setNbAction(PlateauMonstre.getNbAction() + 5);
		PlateauChasseur.setObusDispo(true);
		PlateauChasseur.setFouilleDispo(false);

	}

}
