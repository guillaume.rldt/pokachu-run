package objet;

import projet.PlateauMonstre;

/**
 * Cet objet rajoute une action au monstre pendant 1 tour
 * @author guyardc
 *
 */

public class CafeM extends ObjetMonstre {
	private final static int DUREE = 1;
	/**
	 * constructeur super
	 */
	public CafeM() {
		super(DUREE);
	}
	/**
	 * execute l'objet
	 */
	public void execute() {
		PlateauMonstre.setNbAction(PlateauMonstre.getNbAction() + 1);
	}
}
