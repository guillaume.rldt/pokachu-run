package objet;

import projet.PlateauChasseur;

/**
 * Cet objet permet au chasseur d'avoir une action en plus pendant 1 tour
 * @author guyardc
 *
 */

public class CafeC extends ObjetChasseur {
	private final static int DUREE = 1;
	/**
	 * constructeur super
	 */
	public CafeC() {
		super(DUREE);
	}
	/**
	 * execute l'objet
	 */
	public void execute() {
		PlateauChasseur.setNbAction(PlateauChasseur.getNbAction() + 1);
	}
}
